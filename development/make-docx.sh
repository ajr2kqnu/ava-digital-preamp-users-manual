#!/bin/bash

# Convert all markdown files in working dir to pdf.
# Spaces in filenames not allowed.

INPUT_DOC=digipre-manual-text
MARKDOWN_EXT=md
OFFLINE="--offline"
#~ OPTIONS="-Ssm"      # most have been removed
#~ OPTIONS="--toc -Sm" # most have been removed
EXTENSIONS="+smart"

OPTIONS="${OPTIONS} $@"

pandoc -t docx $OPTIONS -o $INPUT_DOC.docx $INPUT_DOC.$MARKDOWN_EXT

