# DVA Digital Preamplifier User's Guide
[Audio by Van Alstine](http://avahifi.com)  
[avahifi.com](http://avahifi.com)  
June 17, 2021

© Copyright 2021, Audio by Van Altsine, LLC. All rights reserved.

# Quick start

![Touchscreen display's touch areas](../image-touchscreen/digipre-touchscreen-annotated.svg "Touchscreen display's touch areas")

If you plan to connect your  DVA Digital Preamplifier to a computer running Microsoft Windows, install the software driver on your computer as described in **Software driver** below. Do this before connecting the computer to the  DVA Digital Preamplifier.

1. Make sure your audio system is off.
2. Connect your digital sources to the appropriate USB, coaxial, and optical inputs of the DVA Digital Preamplifier.
3. Connect the DVA Digital Preamplifier's RCA or XLR outputs to a power amplifier. If you are using the DVA Digital Preamplifier as a conventional DAC, connect the outputs to an analog preamplifier or integrated amplifier.
4. Plug one end of the provided IEC power cable into the power socket on the Digital Preamplifier and the other end into an earthed (i.e., grounded) mains wall outlet.
5. Power up the DVA Digital Preamplifier using the only button on the faceplate or the ⏻ button on the included remote control. **Don’t power up the rest of the system yet!**
6. Select the desired input by touching the upper left or lower left of the touchscreen to scan up or down through the inputs or by using the INPUT buttons on the remote control.
7. Adjust the volume level up or down by touching the upper right or lower right of the touchscreen display or by using the VOLUME buttons on the remote control.
    * If you are connecting the DVA Digital Preamplifier directly to a power amplifier, set the level to zero to avoid unexpectedly loud output. Once the remainder of the system is on, increase the output from there.
    * If you are connecting the DVA Digital Preamplifier to an analog preamplifier, you will get the best results by turning the volume all the way up and leaving it there.
8. Power on the remainder of the system.
9. Enjoy!
10. To power off the DVA Digital Preamplifier, use the button on the faceplate or the ⏻ button on the remote control.

--------------------------------------------------------------------------------

# Introduction

The DVA Digital Preamplifier is a stereophonic digital to analog converter that lets you control the volume control of the output signal. It can be connected directly to a power amplifier, thereby bypassing any colorations an analog preamplifier may have on the sound. It can also be used as a conventional DAC by setting the volume control to its maximum level.

# Software driver

The DVA Digital Preamplifer uses the highly regarded Amanero Combo384 to decode USB audio. For best results on computers running Microsoft Windows, install the most recent Combo384 driver from [https://www.amanero.com/drivers.htm](https://www.amanero.com/drivers.htm). No additional drivers are required for macOS and most Linux systems.

# Remote control

The included remote control has buttons to **power** the unit on and off, select the **input source**, and adjust the **volume level**. In addition, it provides the following functions not available through the front panel.

The button labeled with a **crossed-out speaker** will mute and unmute the output. Changing the volume will also unmute the output if it is muted.

The button labeled **F** will let you choose from four display fade levels: maximum brightness (no fade), normal brightness (the default), low brightness, and Cinema Mode (maximum fade). Cinema Mode produces a very dim display that's meant to be just bright enough to be visible in a darkened room but not so bright as to be distracting.

# Inputs

The DVA Digital Preamplifier has five sets of inputs as shown in the following table.

| Input       | Display  | Description                |
| :---------- | :------- | :------------------------- |
| USB         | USB      | USB 2.0 Type B             |
| COAX 1      | D1       | RCA coaxial S/PDIF         |
| COAX 2      | D2       | RCA coaxial S/PDIF         |
| OPTICAL 1   | D3       | Optical S/PDIF (full size) |
| OPTICAL 2   | D4       | Optical S/PDIF (full size) |

The galvanically isolated USB input supports PCM digital audio up to and including 192kHz as well as DSD64, DSD128, and DSD256. As of this writing, the maximum DSD rate on macOS is limited to DSD128.

The S/PDIF inputs accept AES/EBU consumer digital audio streams. The coaxial RCA S/PDIF inputs are transformer isolated and support PCM digital audio up to and including 192kHz sample rates. To connect these inputs, use cables designed for 75Ω applications.

The optical S/PDIF inputs are compatible with full-size TOSLINK-style cables and support PCM digital audio up to and including 96kHz sample rates. In some cases, these inputs may work with 192kHz PCM sources, but this is outside the normal specifications for TOSLINK digital audio connections.

# Outputs

The DVA Digital Preamplifier has both single-ended RCA outputs and differentially balanced XLR outputs.

## RCA outputs
The RCA outputs will have a full-scale level of **TBD** volts zero-to-peak at maximum volume. These outputs are designed to drive loads of **TBD** Ω or higher.

## XLR outputs
The XLR outputs will have a full-scale level of **TBD** volts zero to peak at maximum volume. The XLR outputs are designed to drive differential loads of **TBD** Ω or higher (i.e., **TBD** Ω to ground per hot/cold output).

While the unit's XLR outputs are balanced, they are not "transformer-like". This means if you ground the cold side of the XLR and connect the hot side as the signal, the net level will be halved and some differential advantages will be lost. If you are connecting the unit to a non-differential or unbalanced XLR input, we recommend you use the RCA outputs with appropriate adapters instead.

# Mains power

The Digital Preamplifier ships with a good quality IEC power cable. An internal filter helps keep RFI and other noise from entering and exiting the unit. We recommend against using "high-end" replacement IEC cables as the likelihood that they will improve system performance is very, very small, and some may negatively impact performance.

## Ghost current
To help address the issue of ghost current, the DVA Digital Preamplifier has a power switch on the back that completely turns off power to the unit. When this switch is in the OFF position, you will not be able to power on the unit for use. When the switch is in the ON position, the front panel power switch or remote control ⏻ button will alternately turn the unit fully on for use or into standby mode.

In standby mode, the unit draws approximately **TBD** mA.

## Fuse

The DVA Digital Preamplifier uses a single 0.5A 3AG slow-blow (a.k.a. Slo-Blo® or time-delay) fuse. Under normal use, the fuse is not expected to blow unless it's several years old. If your fuse blows before this, consider having the unit checked by a professional technician to assure it's free of faults.

To replace a blown fuse, first **unplug the unit**. Then rotate the fuse holder's cap to loosen it, pull the cap and old fuse out, and remove the old fuse from the cap. Then insert the new fuse into the cap, insert and rotate the cap back into the housing, and plug your unit back in.

If the fuse blows quickly, do not continue to use the unit. Consult a professional technician as there is likely a fault in the unit.

# Grounding

Hum and buzz in audio system is frequently caused by poor grounding configurations that introduce ground loops. Proper grounding is especially complicated in differential/balanced systems owing to there being no de facto standard for how XLR connectors are configured. While it's impossible to include a complete article on proper grounding in this guide, the following can help you to resolve common grounding issues.

## Chassis grounding
For proper safety with mains-powered devices where there is any chance of human-metal contact, those metal parts must be connected to mains earth/ground. In the USA, the mains/ground connection is the round pin on a standard wall outlet.

Audio equipment also has a so-called signal ground. It used to be common to hardwire the signal ground and chassis together. However, when you interconnect two (or more) audio devices wired this way that are also properly connected to mains earth, a ground loop results. This can result in the production of hum and buzz in the system.

Further complicating matters, for a unit's metal box to work as an EMI shield, it must be connected to a solid reference. This is almost always mains earth in modern equipment and signal ground in legacy equipment.

All of the above results in a quagmire of conditions you need to meet for safety, eliminating ground loops, and good shielding.

Fortunately, the solution is surprisingly simple:

1. Make sure the chassis of _all_ your audio equipment equipped with three-prong mains cables are connected to mains earth through the power cable. This is an important safety standard.
2. Make sure _only one piece of equipment_ in your system connects its signal ground to its chassis (and therefore mains earth).

The DVA Digital Preamplifier's chassis is permanently connected to mains earth at the power inlet, which satisfies the first requirement. To help you satisfy the second, the DVA Digital Preamplifier has a SIGNAL GROUND switch on the back. When it's in the OFF position, the unit's signal ground is not connected to the chassis. When in the ON position, it is.

You don't need to worry about the DVA Digital Preamplifier's input connections introducing ground loops. All digital inputs on the unit are fully isolated, meaning there is no direct connection between input source grounds and the internal signal ground.

**Do not use "cheater" adapters to help address ground loop issues. Doing so can cause safety issues.**

## XLR grounding
Your DVA Digital Preamplifier ships with pin 1 of the XLR jacks connected to signal ground and the XLR shields connected to the unit’s chassis. This will work well for many XLR-based systems in conjunction with the SIGNAL GROUND switch discussed above.

### XLR options for technically advanced users
Depending on how the XLR jacks are configured in the connected equipment and how your XLR cables are wired, the default configuration discussed above may lead to ground loops or other issues. For this reason, you can further refine the DVA Digital Preamplifier’s grounding scheme via a DIP switch inside the unit.

**Do not proceed unless you are comfortable working inside mains-powered devices.**

To access the DIP switch, first remove the IEC cable from the unit. Then remove the **TBD** 10? screws holding the top in place. You should then be able to slide the top off from the back of the unit.

**TBD**: Image of DIP switch

The first switch position connects XLR pin 1 on both channels to signal ground. The default position for this switch is ON. The second position connects pin 1 to the chassis and cable shield. The default position for this switch is OFF. You can use a small tool like a round toothpick, precision screwdriver, or spludger to set these as needed for your related equipment and interconnects.

# Transferable Limited Three-Year Warranty

This Audio by Van Alstine designed and built product carries a limited three-year warranty on parts and labor, subject to the following exceptions and conditions:

* AUDIO BY VAN ALSTINE SHALL NOT BE LIABLE UNDER THIS WARRANTY IF ANY DAMAGE OR DEFECT RESULTS FROM (I) MISUSE, ABUSE, NEGLECT, IMPROPER SHIPPING OR INSTALLATION; (II) DISASTERS SUCH AS FIRE, FLOOD, LIGHTNING OR IMPROPER ELECTRIC CURRENT; OR (III) SERVICE OR ALTERATION BY ANYONE OTHER THAN AUDIO BY VAN ALSTINE.
* Repairs, including out-of-warranty repairs on AVA equipment, have a 90-day limited warranty.
* This limited warranty is transferable from the original buyer to one subsequent owner. That second owner must furnish a written sales receipt signed by the original owner to certify the transfer of ownership of the equipment and to be eligible for the balance of the limited warranty.
* You must give us a chance to help you by calling us at 651-330-9871 for return authorization before returning equipment to us. The best service for you is when we can identify and solve a use problem by phone, resulting in no need for a repair at all.
